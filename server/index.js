console.log("Implement servermu disini yak 😝!");
const express = require('express');
const path = require('path');

const app = express();
// const port = process.env.PORT || 8080;
app.use(express.static('public'))

// sendFile will go here
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});
app.get('/cars', function(req, res) {
  res.sendFile(path.join(__dirname, '../public/cars.html'));
});
app.get('/carsnooop', function(req, res) {
  res.sendFile(path.join(__dirname, '../public/cars-no-oop.html'));
});
app.get('/example', function(req, res) {
  res.sendFile(path.join(__dirname, '../public/index.example.html'));
});

app.listen(3000, '0.0.0.0', ()=>{
  console.log(__dirname)
  console.log('Server started at http://0.0.0.0:%d', 3000);
})